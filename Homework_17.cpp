﻿#include <iostream>
#include <cmath>

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;

public:
    Vector()
    {}

    Vector(double x, double y, double z): x(x), y(y), z(z)
    {}

    double getX()
    {
        return x;
    }

    double getY()
    {
        return y;
    }

    double getZ()
    {
        return z;
    }

    double getLength()
    {
        return sqrt(x * x + y * y + z * z);
    }

};

int main()
{
    Vector v(10, 12, 15);

    std::cout << v.getX() << ' ' << v.getY() << ' ' << v.getZ() << std::endl;
    std::cout << v.getLength() << std::endl;
}

